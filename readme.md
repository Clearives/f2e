##前端学习轨迹

### 重构

我个人建议，作为前端，有必要掌握重构，那个东西多写就会了，可以这么说，具体包括下面的一些东西：
- HTML

(这个我就不多说了，不会就看文档，死的东西)

- CSS
这个部分，我觉得盒子模型，定位，已经各种花式布局，这样说吧，就是当你看到任何一张页面你都能快速合理的下手去写出来，那么这一块基本上没什么问题了，当然可以深入的东西也很多，自己感兴趣可以深入了解各种玩法（盒子模型和定位对于布局很重要，理解了就很简单）


- JS

这部分说重构，那么js用的其实不多，基本的语法掌握一下就OK。

>这部分推荐资料：

- 《禅意花园》
- 《css网站布局实录》
- 《精通css》   
- [比W3c更好的网站我觉得](http://www.runoob.com/) (我个人觉得看这个就够了，有时间你看以买本书翻一翻)



### Javascript(这部分单独拿出来说)
现在的前端生态，你们千万别认为js就是写一些动画操作什么的，如果你还这么理解，那么你有必要重新认识一下JS

- 基本语法

- 变量

- 数据类型

- 函数

- 运算符及流程控制

- JS对象

- DOM操作

- 面向对象（重点，重点，重点）

- 事件对象（重点）

- AJAX

> 上面的这些东西是JS的基础知识，也就是说你必须先了解他们，还有就是，这不是一时半会能全部掌握的，是要时间和经验的沉淀，包括我，也还是要不断的学习，因为涉及的东西真的很多很多，这里强烈推荐两本书，一本就是《JS高级程序设计》《javascript学习指南》，随便买一本，我个人买的前面这一本，我好像有两本，哈哈，一本在家，这书也可以当作手册，在你没事的时候可以翻翻，真的不错《DOM编程的艺术》这本书也不错


> 网上的资料的话全在电脑上，没怎么整理，到时候整理一份出来，W3c还是很推荐的，其他的当你学着学着其实多会发现，前端的社区还是很不错的。


### 移动端（H5）

最近两年，移动端真的是很火很火，我本人是从毕业到15年底都是在做移动端，学了很多东西，踩了很多坑

- 移动端布局

这个其实和前端的布局差别不是很多，就是新增了很多写法，比如响应式布局，百分比布局，flex这些，这里强烈推荐瓢虫书，《响应式web设计》，这本书很薄，花点时间应该很快可以看完

- JS方面
这个时候，jq啊，zepto啊这些库，希望你能用。没事去看看源码，移动端，涉及的东西很多，你可以多去了解一下，这里还建议大家把一下能公用的JS抽出来，要有模块的思想。总的来说，就是多接触新的技术，那样你会发现H5玩法真的很多。

- css3

还是一样，先看文档，过一遍再说。然后里面一些新的属性啊，我玩css3最多的还是动画，真的是玩很多

- canvas

有兴趣的话可以去玩一下，没错，这个东西可以做游戏，但是要用JS,真的很吊，性能也很好。有一些引擎啊，比如白鹭引擎，createjs等等


> 这里我感觉还是要好好学jS,js是一定要持续性学习的。其它的我觉得不是很难，就是坑要自己去踩，踩过了才会记得下次不去踩。


###### 推荐资料及网站

- [大漠的网站]{http://www.w3cplus.com/}  特别推荐

- [腾讯alloyteam团队博客]{http://www.alloyteam.com/}

- [淘宝UED]{http://ued.taobao.org/blog//}

- [腾讯isux]{http://isux.tencent.com/}

- [腾讯isux css3]{http://isux.tencent.com/css3/}  这个很棒


这个期间你可以学习bootstrap，less, sass,isroll，swiper等等这些都可以去接触。



### 继续

这里可能你慢慢开始用框架了，比如当前很火的react，我也是刚用不久这个，之前用的是Avalon

- 模块化(CMD与AMD)

对应这两种模式的就是seajs与requirejs，个人感觉就是打包问题上前者有时候很蛋疼，其它还好


- 构建工具（gulp/grunt,当然我推荐的是webapack）

我玩过gulp，很叼，但是现在搞react，就开始用webpack了，这些说实话我不是很熟，网上资料也很多可以参考网上的资料学习。


- MVVM框架

是时候学一款好的前端框架了，学会了一种，其它的上手的话应该不是特别的难。
比如现在的流行的react+redux+webpack,也可以学习angularjs，现在是1.X版本，2.0会大改，可以慢慢搞，还有比如vue啊什么的都挺火的现在。选一款合适的好好研究吧。


这个时候，还是那句话，JS很重要，很重要，很重要。你可以看看设计模式什么的啊。

我还没到那个程度，但是下面的东西还是可以大致的说一下。

> 这个阶段可以看一下设计模式，编程思想，之类的数据，研究开源的代码。注意自己代码的质量。


### 编程思想、架构能力

毕业前几年，你可能真的就是一个码农，天天就是做那写代码。写bug。要想在技术层面有所成就，那么是时候看你的编程能力和素养了，不是看你能不能写，而是看你能不能写好。架构能力什么的，包括团队能力，这些都是你工作几年后要考虑的东西，当然像我，迟早要回家开饭店的，所以我暂时也不想想太多。哈哈。
可能对于我来说，大学不是学的计算机，连计算机二级证书考试都是抄别人的，我真的基础很差，我现在就是好多东西玩的不深，但是接触的东西很多。包括有领导跟我讲过，职业规划很重要，你可以选择横向发展也可以选择纵向发展，这些都没有问题，问题是你必须选一个方向，不管什么行业都是那样。我其实真的很讨厌写代码。可是我目前还没有勇气去做别的。所以还是老老实实的学习。






































